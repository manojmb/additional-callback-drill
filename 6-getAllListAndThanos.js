const fs = require('fs')

const { getBoardInfo } = require("./1-findBoardInformation.js")
const { getListBelongToBoard } = require("./2-findAllListBelongToBoard.js")
const { getCardsBelongToList } = require('./3-findAllCards.js')

function getAllListAndThanos(path, callback) {
    [boardPath, listPath, cardPath] = path
    setTimeout(() => {
        fs.readFile(boardPath, 'utf8', (err, data) => {
            if (err) {
                callback(err, null);
                return;
            }
            else {
                const boards = JSON.parse(data);
                let targetBoardID = ""
                boards.forEach((element) => {
                    if (element['name'] === "Thanos") {
                        targetBoardID = element['id']
                    }
                })

                getBoardInfo(boardPath, targetBoardID, (err, boardInfo) => {
                    if (err) {
                        return callback(err)
                    } else {
                        getListBelongToBoard(listPath, targetBoardID, (err, listInfo) => {
                            if (err) {
                                return callback(null, err);
                            } else {
                                let mindID = ""
                                let spaceID = "";
                                // console.log(listInfo)
                                listInfo.forEach((element) => {
                                    if (element['name'] === "Mind") {
                                        mindID = element['id'];
                                    }
                                    if (element['name'] === "Space") {
                                        spaceID = element['id'];
                                    }
                                });
                                getCardsBelongToList(cardPath, mindID, (err, cardInfo) => {
                                    if (err) {
                                        return callback(null, err);
                                    } else {
                                        // console.log('Card Information:', cardInfo);
                                        // callback(cardInfo)

                                        fs.readFile(listPath, 'utf8', (err, data) => {
                                            if (err) {
                                                return callback(null, err);

                                            }
                                            else {
                                                const lists = JSON.parse(data);
                                                listId = Object.keys(lists).reduce((acc, key) => {
                                                    lists[key].forEach((item) => {
                                                        acc.push(item['id'])
                                                    })
                                                    return acc
                                                }, [])

                                                // console.log(listId)

                                                listId.forEach((id) => {
                                                    getCardsBelongToList(cardPath, id, (err, cardInfo) => {
                                                        if (err) {
                                                            // console.error('Error:', err);

                                                            return callback(null, err)

                                                        } else {
                                                            // arr.push(cardInfo)
                                                            console.log('Card Information:', cardInfo);

                                                        }
                                                    });
                                                })

                                                callback(null, { boardInfo, listInfo })
                                            }
                                        })


                                    }
                                });
                            }
                        });

                    }
                });
            }
        })

    }, parseInt(Math.random() * 10000, 10))
}

module.exports = { getAllListAndThanos }