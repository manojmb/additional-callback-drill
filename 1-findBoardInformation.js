const fs = require('fs')

Math.random() * 10 ** 4

function getBoardInfo(boardPath, boardID, callback) {
    setTimeout(() => {
        fs.readFile(boardPath, 'utf8', (err, data) => {
            if (err) {
                callback(err, null);
                return;
            }
            else {
                try {
                    const boards = JSON.parse(data);

                    // Find the board with the matching boardID
                    const foundBoard = boards.find(board => board.id === boardID);

                    if (foundBoard) {
                        // If a board is found, pass its information to the callback
                        callback(null, foundBoard);
                    } else {
                        // If no board is not found, indicate an error in the callback
                        callback('Board not found', null);
                    }
                } catch (parseError) {
                    // Handle JSON parsing errors
                    callback(parseError, null);
                }
            }


        });
    }, parseInt(Math.random()*10000,10));

}

module.exports = { getBoardInfo }
