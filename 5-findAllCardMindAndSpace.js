const fs = require('fs');
const { getBoardInfo } = require("./1-findBoardInformation.js");
const { getListBelongToBoard } = require("./2-findAllListBelongToBoard.js");
const { getCardsBelongToList } = require('./3-findAllCards.js');

function findAllCardMindAndSpace(path, callback) {
    const [boardPath, listPath, cardPath] = path;

    setTimeout(() => {
        fs.readFile(boardPath, 'utf8', (err, data) => {
            if (err) {
                return callback(err, null);
            }

            const boards = JSON.parse(data);
            let targetBoardID = "";

            for (const element of boards) {
                if (element['name'] === "Thanos") {
                    targetBoardID = element['id'];
                    break;
                }
            }

            getBoardInfo(boardPath, targetBoardID, (err, boardInfo) => {
                if (err) {
                    return callback(err, null);
                }

                getListBelongToBoard(listPath, targetBoardID, (err, listInfo) => {
                    if (err) {
                        return callback(err, null);
                    }

                    let mindID = "";
                    let spaceID = "";

                    for (const element of listInfo) {
                        if (element['name'] === "Mind") {
                            mindID = element['id'];
                        }
                        if (element['name'] === "Space") {
                            spaceID = element['id'];
                        }
                    }

                    getCardsBelongToList(cardPath, mindID, (err, mindCardInfo) => {
                        if (err) {
                            return callback(err, null);
                        }

                        getCardsBelongToList(cardPath, spaceID, (err, spaceCardInfo) => {
                            if (err) {
                                return callback(err, null);
                            }

                            return callback(null, { boardInfo, mindCardInfo, spaceCardInfo });
                        });
                    });
                });
            });
        });
    }, parseInt(Math.random() * 10000, 10));
}

module.exports = { findAllCardMindAndSpace };
