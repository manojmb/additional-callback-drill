const fs = require('fs')

function getCardsBelongToList(cardPath, listId, callback) {
    setTimeout(() => {
        fs.readFile(cardPath, 'utf8', (err, data) => {
            if (err) {
                callback(err, null);
                return;
            }
            else {
                try {
                    const cards = JSON.parse(data);

                    // Find the board with the matching listId
                    const foundCard = cards[listId];

                    if (foundCard) {
                        // If a board is found, pass its information to the callback
                        callback(null, foundCard);
                    } else {
                        // If no board is not found, indicate an error in the callback
                        callback('Cards not found', null);
                    }
                } catch (parseError) {
                    // Handle JSON parsing errors
                    callback(parseError, null);
                }
            }


        });
    }, parseInt(Math.random() * 10000, 10));

}

module.exports = { getCardsBelongToList }
