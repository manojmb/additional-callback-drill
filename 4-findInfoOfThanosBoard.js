const fs = require('fs');

const { getBoardInfo } = require("./1-findBoardInformation.js");
const { getListBelongToBoard } = require("./2-findAllListBelongToBoard.js");
const { getCardsBelongToList } = require('./3-findAllCards.js');


function findInfoOfThanosBoard(path, callback) {
    const [boardPath, listPath, cardPath] = path;

    setTimeout(() => {
        fs.readFile(boardPath, 'utf8', (err, data) => {
            if (err) {
                return callback(err, null);
            }

            const boards = JSON.parse(data);
            let targetBoardID = "";

            boards.forEach((element) => {
                if (element['name'] === "Thanos") {
                    targetBoardID = element['id'];
                }
            });

            getBoardInfo(boardPath, targetBoardID, (err, boardInfo) => {
                if (err) {
                    return callback(err, null);
                }

                getListBelongToBoard(listPath, targetBoardID, (err, listInfo) => {
                    if (err) {
                        return callback(err, null);
                    }

                    let mindID = "";
                    listInfo.forEach((element) => {
                        if (element['name'] === "Mind") {
                            mindID = element['id'];
                        }
                    });

                    getCardsBelongToList(cardPath, mindID, (err, cardInfo) => {
                        if (err) {
                            return callback(err, null);
                        }

                        return callback(null, { boardInfo, listInfo, cardInfo });
                    });
                });
            });
        });
    }, parseInt(Math.random() * 10000, 10));
}

module.exports = { findInfoOfThanosBoard };