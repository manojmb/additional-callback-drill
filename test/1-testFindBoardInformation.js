const { getBoardInfo } = require("../1-findBoardInformation.js")
const targetBoardID = 'abc122dc';
const boardPath = "../boards_1.json"
getBoardInfo(boardPath, targetBoardID, (err, boardInfo) => {
    if (err) {
        console.error('Error:', err);
    } else {
        console.log('Board Information:', boardInfo);
    }
});