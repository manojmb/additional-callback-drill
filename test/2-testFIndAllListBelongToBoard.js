const { getListBelongToBoard } = require("../2-findAllListBelongToBoard.js")
const targetBoardID = 'abc122dc';
const listPath = "../lists_1.json"
getListBelongToBoard(listPath, targetBoardID, (err, listInfo) => {
    if (err) {
        console.error('Error:', err);
    } else {
        console.log('List Information:', listInfo);
    }
});