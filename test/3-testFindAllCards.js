const { getCardsBelongToList } = require('../3-findAllCards.js')
// const targetListID = 'abc122dc';
const targetListID = "qwsa221"
const cardPath = "../cards_1.json"

getCardsBelongToList(cardPath, targetListID, (err, cardInfo) => {
    if (err) {
        console.error('Error:', err);
    } else {
        console.log('Card Information:', cardInfo);
    }
});