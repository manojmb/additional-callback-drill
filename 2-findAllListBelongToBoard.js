const fs = require('fs')

function getListBelongToBoard(listPath, boardID, callback) {
    setTimeout(() => {
        fs.readFile(listPath, 'utf8', (err, data) => {
            if (err) {
                callback(err, null);
                return;
            }
            else {
                try {
                    const lists = JSON.parse(data);

                    // Find the board with the matching boardID
                    const foundList = lists[boardID];

                    if (foundList) {
                        // If a board is found, pass its information to the callback
                        callback(null, foundList);
                    } else {
                        // If no board is not found, indicate an error in the callback
                        callback('List not found', null);
                    }
                } catch (parseError) {
                    // Handle JSON parsing errors
                    callback(parseError, null);
                }
            }


        });
    }, parseInt(Math.random() * 10000, 10));

}

module.exports = { getListBelongToBoard }
